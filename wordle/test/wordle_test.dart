import 'package:wordle/wordle.dart';
import 'package:test/test.dart';

void main() {
  group('coincidencias de letras para juan', () {
    test('si es julio', () {
      var palabra1 = initLetters("juan");
      var palabra2 = initLetters("julio");
      expect(
          exactlyCoincidences(palabra1, palabra2), initLetters("ju").toSet());
    });
    test('si es si es joas', () {
      var palabra1 = initLetters("juan");
      var palabra2 = initLetters("jias");
      List<Prueba> i = [];
      i.add(Prueba(numero: 0, letra: "j"));
      i.add(Prueba(numero: 2, letra: "a"));
      expect(exactlyCoincidences(palabra1, palabra2), i.toSet());
    });
    test('si es si es joen', () {
      var palabra1 = initLetters("juan");
      var palabra2 = initLetters("joen");
      List<Prueba> i = [];
      i.add(Prueba(numero: 0, letra: "j"));
      i.add(Prueba(numero: 3, letra: "n"));
      expect(exactlyCoincidences(palabra1, palabra2), i.toSet());
    });
    test('si es aaron', () {
      var palabra1 = initLetters("juan");
      var palabra2 = initLetters("aaron");
      List<Prueba> i = [];
      i.add(Prueba(numero: 2, letra: "a"));
      i.add(Prueba(numero: 3, letra: "n"));
      expect(
          notExactlyCoincidences(
              palabra1, palabra2, exactlyCoincidences(palabra1, palabra2)),
          i);
    });
  });

  group('coincidencias no exactas para juan', () {
    test('si es jona', () {
      var palabra1 = initLetters("juan");
      var palabra2 = initLetters("jona");
      List<Prueba> i = [];
      i.add(Prueba(numero: 3, letra: "n"));
      i.add(Prueba(numero: 2, letra: "a"));
      expect(
          notExactlyCoincidences(
              palabra1, palabra2, exactlyCoincidences(palabra1, palabra2)),
          i);
    });
  });
}
